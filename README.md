# conangit

Tool that let you do stuff with conan packages hosted on git repositories.

# Example

If you want to build library `box2d` using recepie from https://github.com/conan-community/conan-box2d,
you need to run following command:

```
cg do https://github.com/conan-community/conan-box2d -- create . user/channel
```

It will clone this repository with recepie, and call conan command to build the package.
