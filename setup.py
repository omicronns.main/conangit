import setuptools

setuptools.setup(
    name="conangit",
    version="0.1",
    description="conangit",
    url="https://gitlab.com/omicronns.main/conangit.git",
    author=["Konrad Adasiewicz"],
    author_email=["omicronns@gmail.com"],
    license="",
    packages=["conangit"],
    install_requires=["click"],
    entry_points={
        "console_scripts": [
            "cg=conangit.conangit:conangit",
        ],
    })
