import os
import re
import subprocess
import shutil
import click


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
def conangit():
    pass


@conangit.command(context_settings=CONTEXT_SETTINGS)
@click.argument("url")
@click.option("--cache-dir", "-d", type=str, help="Directory to use instead $HOME/.conangit/cache.")
@click.option("--cache-name", "-n", type=str, help="Clone folder name.")
@click.option("--git-ref", "-b", type=str, help="Branch or tag to checkout.")
@click.option("--full", "-f", is_flag=True, help="Checkout full repository instead single branch.")
@click.argument("conan_params", type=str, nargs=-1)
def do(url, cache_dir, cache_name, git_ref, full, conan_params):
    """ Make some conan job for you. """
    git_regex = re.compile(r"(?:git|ssh|https?|git@[-\w.]+):(\/\/)?(.*?)(\.git)(\/?|\#[-\d\w._]+?)$")
    if not git_regex.match(url):
        print("Could not recognize url as git repository, sorry.")
        return
    repo_name = git_regex.search(url).group(2).split("/")[-1]
    cache_dir = cache_dir if cache_dir else "{}/.conangit/cache".format(os.path.expanduser("~"))
    cache_name = cache_name if cache_name else repo_name + "_" + git_ref if git_ref else repo_name
    cache_dir += "/" + cache_name
    if not os.path.exists(cache_dir):
        git_command = ["git", "clone"]
        if git_ref:
            git_command.append("--branch")
            git_command.append(git_ref)
        if not full:
            git_command.append("--single-branch")
        git_command.append(url)
        git_command.append(cache_dir)
        try:
            subprocess.check_call(git_command)
        except:
            print("Conanfile repository clone failed.")
    if conan_params:
        try:
            os.chdir(cache_dir)
            subprocess.check_call(["conan"] + list(conan_params))
        except:
            print("Conan command failed.")


@conangit.command(context_settings=CONTEXT_SETTINGS)
def clean():
    """ Cleans cache repository. Does not remove packages. """
    cache_dir = "{}/.conangit/cache".format(os.path.expanduser("~"))
    if os.path.exists(cache_dir):
        shutil.rmtree(cache_dir)
    else:
        print("cache empty")
